'''
Input: [Season(Spring,Fall,Winter,Summer)][trail][Elevation dataset][Terrain]
'''
import math,sys
from Node import Node
from PIL import Image, ImageDraw
# Holds speed values across different terrains
D = {}
# Image object
im = Image.open(sys.argv[4])
w, h = 500, 500
# Matrix to hold elevation data
Elevation = [[0 for x in range(w)] for y in range(h)]
def main():
    """
    Does pre-processing and displays best path
    :return:
    """
    global D
    global im,Elevation
    im = im.convert('RGB')
    f = open(sys.argv[3])
    content = list(f)
    #read elevation data into Elevation matrix
    for i in range(0,500):
        s = content[i].strip()
        s = s[0:len(s)-80]
        k = 0
        for j in range(0,len(s),16):
            if s[j] == ' ':
                continue
            Elevation[i][k] = float(s[j:j+13])
            k+=1
    # Pre-processing for Fall
    if sys.argv[1] == 'Fall':
        for x in range(0,395):
            for y in range(0,500):
                if im.getpixel((x,y)) == (255,255,255):
                    im.putpixel((x,y),(227,207,87))
    # Pre-processing for Winter
    if sys.argv[1] == 'Winter':
        pix = list()
        for x in range(0,395):
            for y in range(0,500):
                if im.getpixel((x, y)) == (0,0,255) and pixneighbor(x,y,(0,0,255),"Winter"):
                    pix.append((x,y))
        BFS(pix,"Winter")
    # Pre-processing for Spring
    if sys.argv[1] == 'Spring':
        pix = list()
        for x in range(0,395):
            for y in range(0,500):
                if im.getpixel((x, y)) == (0,0,255) and pixneighbor(x,y,(0,0,255),"Winter"):
                    pix.append((x,y))
        BFS(pix,"Spring")

    # Assigning speed values across different terrains
    D[(248,148,18)] = 10
    D[(255,192,0)] = 9
    D[(255,255,255)] = 7
    D[(2,208,60)] = 6
    D[(2,136,40)] = 5
    D[(5,73,24)] = 3
    D[(0,0,255)] = 2
    D[(71,51,3)] = 11
    D[(0,0,0)] = 12
    D[(205,0,101)] = 1
    D[(227,207,87)] = 6.5
    D[(193,205,205)] = 3.5
    D[(0,0,254)] = 5.5

    # Read trail file and execute A star search to find path
    f1 = open(sys.argv[2])
    points = list(f1)
    for i in range(1,len(points)):
        s1 = points[i-1].strip()
        s2 = points[i].strip()
        x1 = s1[0:3]
        y1 = s1[4:7]
        x2 = s2[0:3]
        y2 = s2[4:7]
        s = Node(int(x1), int(y1),None)
        g = Node(int(x2), int(y2),None)
        path = Astar(s,g)
        draw = ImageDraw.Draw(im)
        n = path[0]
        d = 0.0
        # Draw path and calculate total distance
        for p in path:
            draw.line((n.x, n.y) + (p.x, p.y), fill=128)
            d = d + math.sqrt(math.pow(((n.x - p.x) * 10.29), 2) + math.pow(((n.y - p.y) * 7.55), 2))
            n = p
    print("Total distance travelled: ",d)
    im.show()

def Astar(start,goal):
    """
    Implementation of a-star search algorithm
    :param start: Starting point
    :param goal: End point
    :return:
    """
    open_list = [start]
    closed_list = []
    start.tt = 0
    start.ht = start.tt + heuristic(start,goal)

    while open_list:
        current = open_list[0]
        if current.x == goal.x and current.y == goal.y:
            return constructpath(current)
        open_list.remove(current)
        closed_list.append(current)
        nbrs = neighbors(current)
        for i in nbrs:
            f = False
            for c in closed_list:
                if c.x == i.x and c.y == i.y:
                    f = True
                    break
            if not f:
                i.ht = i.tt + heuristic(i,goal)
                for o in open_list:
                    if o.x == i.x and o.y == i.y:
                        f = True
                        break
                if not f:
                    open_list.append(i)
                    open_list.sort(key=lambda x:x.ht , reverse=False)
                else:
                    for n in open_list:
                        if n.x == i.x and n.y == i.y and i.tt < n.tt:
                            n.tt = i.tt
                            n.parent = i.parent
    return False

def BFS(p,s):
    """
    Implements breadth first search to process image based on the season
    :param p: Points from where to start BFS
    :param s: Season
    :return:
    """
    global im
    if s == 'Winter':
        # For 7 pixels from non-water pixel
        for i in range(0,7):
            # List containing child
            c = list()
            # Change parent pixel
            for j in p:
                im.putpixel(j,(193,205,205))
                # Find child uptill level 6
                if(i < 6):
                    if j[0] > 0 and j[1] > 0 and im.getpixel((j[0] - 1, j[1] - 1)) == (0,0,255):
                        c.append((j[0] - 1, j[1] - 1))
                    if j[0] > 0 and im.getpixel((j[0] - 1, j[1])) == (0,0,255):
                        c.append((j[0] - 1, j[1]))
                    if j[0] > 0 and j[1] < 499 and im.getpixel((j[0] - 1, j[1] + 1)) == (0,0,255):
                        c.append((j[0] - 1, j[1] + 1))
                    if j[1] > 0 and im.getpixel((j[0], j[1] - 1)) == (0,0,255):
                        c.append((j[0], j[1] - 1))
                    if j[1] < 499 and im.getpixel((j[0], j[1] + 1)) == (0,0,255):
                        c.append((j[0], j[1] + 1))
                    if j[0] < 394 and j[1] < 499 and im.getpixel((j[0] + 1, j[1] + 1)) == (0,0,255):
                        c.append((j[0] + 1, j[1] + 1))
                    if j[0] < 394 and im.getpixel((j[0] + 1, j[1])) == (0,0,255):
                        c.append((j[0] + 1, j[1]))
                    if j[0] < 394 and j[1] > 0 and im.getpixel((j[0] + 1, j[1] - 1)) == (0,0,255):
                        c.append((j[0] + 1, j[1] - 1))
            # Copy child list in parent
            p = c
    if s=='Spring':
        # Holds elevation difference between edge of water pixel to child nodes moving out from there to non-water pixels
        sprng = [ [0] * 500 for _ in range(500)]
        # For 15 pixels
        for i in range(0,15):
            c = list()
            for j in p:
                # Find child node
                if(i < 14):
                    # Conditions to check pixel is not water but land and not out of bounds
                    if j[0] > 0 and j[1] > 0  and im.getpixel((j[0] - 1, j[1] - 1)) != (205,0,101) and im.getpixel((j[0] - 1, j[1] - 1)) != (0,0,255)and im.getpixel((j[0] - 1, j[1] - 1)) != (0,0,254):
                        # Calculate total Elevation difference
                        sprng[j[0] - 1][j[1] - 1] = sprng[j[0]][j[1]] + Elevation[j[0] - 1][j[1] - 1] - Elevation[j[0]][j[1]]
                        if sprng[j[0] - 1][j[1] - 1] <= 1:
                            c.append((j[0] - 1, j[1] - 1))
                            im.putpixel((j[0] - 1, j[1] - 1),(0,0,254))
                    if j[0] > 0 and im.getpixel((j[0] - 1, j[1])) != (205,0,101) and im.getpixel((j[0] - 1, j[1])) != (0,0,255)and im.getpixel((j[0] - 1, j[1])) != (0,0,254) and im.getpixel((j[0] - 1, j[1])) != (205,0,101):
                        sprng[j[0]-1][j[1]] = sprng[j[0]][j[1]] + Elevation[j[0]-1][j[1]] - Elevation[j[0]][j[1]]
                        if sprng[j[0]-1][j[1]] <= 1:
                            c.append((j[0] - 1, j[1]))
                            im.putpixel((j[0] - 1, j[1]),(0,0,254))
                    if j[0] > 0 and j[1] < 499 and im.getpixel((j[0] - 1, j[1] + 1)) != (205,0,101) and im.getpixel((j[0] - 1, j[1] + 1)) != (0,0,255)and im.getpixel((j[0] - 1, j[1] + 1)) != (0,0,254):
                        sprng[j[0]-1][j[1]+1] = sprng[j[0]][j[1]] + Elevation[j[0]-1][j[1]+1] - Elevation[j[0]][j[1]]
                        if sprng[j[0]-1][j[1]+1] <= 1:
                            c.append((j[0] - 1, j[1] + 1))
                            im.putpixel((j[0] - 1, j[1] + 1), (0, 0, 254))
                    if j[1] > 0 and im.getpixel((j[0], j[1] - 1)) != (205,0,101) and im.getpixel((j[0], j[1] - 1)) != (0,0,255)and im.getpixel((j[0], j[1] - 1)) != (0,0,254):
                        sprng[j[0]][j[1]-1] = sprng[j[0]][j[1]] + Elevation[j[0]][j[1]-1] - Elevation[j[0]][j[1]]
                        if sprng[j[0]][j[1]-1] <= 1:
                            c.append((j[0], j[1] - 1))
                            im.putpixel((j[0], j[1] - 1), (0, 0, 254))
                    if j[1] < 499 and im.getpixel((j[0], j[1] + 1)) != (205,0,101) and im.getpixel((j[0], j[1] + 1)) != (0,0,255)and im.getpixel((j[0], j[1] + 1)) != (0,0,254):
                        sprng[j[0]][j[1]+1] = sprng[j[0]][j[1]] + Elevation[j[0]][j[1]+1] - Elevation[j[0]][j[1]]
                        if sprng[j[0]][j[1]+1] <= 1:
                            c.append((j[0], j[1] + 1))
                            im.putpixel((j[0], j[1] + 1), (0, 0, 254))
                    if j[0] < 394 and j[1] < 499 and im.getpixel((j[0] + 1, j[1] + 1)) != (205,0,101) and im.getpixel((j[0] + 1, j[1] + 1)) != (0,0,255)and im.getpixel((j[0] + 1, j[1] + 1)) != (0,0,254):
                        sprng[j[0]+1][j[1]+1] = sprng[j[0]][j[1]] + Elevation[j[0]+1][j[1]+1] - Elevation[j[0]][j[1]]
                        if sprng[j[0]+1][j[1]+1] <= 1:
                            c.append((j[0] + 1, j[1] + 1))
                            im.putpixel((j[0] + 1, j[1] + 1), (0, 0, 254))
                    if j[0] < 394 and im.getpixel((j[0] + 1, j[1])) != (205,0,101) and im.getpixel((j[0] + 1, j[1])) != (0,0,255) and im.getpixel((j[0] + 1, j[1])) != (0,0,254):
                        sprng[j[0]+1][j[1]] = sprng[j[0]][j[1]] + Elevation[j[0]+1][j[1]] - Elevation[j[0]][j[1]]
                        if sprng[j[0]+1][j[1]] <= 1:
                            c.append((j[0] + 1, j[1]))
                            im.putpixel((j[0] + 1, j[1]), (0, 0, 254))
                    if j[0] < 394 and j[1] > 0 and im.getpixel((j[0] + 1, j[1] - 1)) != (205,0,101) and im.getpixel((j[0] + 1, j[1] - 1)) != (0,0,255) and im.getpixel((j[0] + 1, j[1] - 1)) != (0,0,254):
                        sprng[j[0]+1][j[1]-1] = sprng[j[0]][j[1]] + Elevation[j[0]+1][j[1]-1] - Elevation[j[0]][j[1]]
                        if sprng[j[0]+1][j[1]-1] <= 1:
                            c.append((j[0] + 1, j[1] - 1))
                            im.putpixel((j[0] + 1, j[1] - 1), (0, 0, 254))
            p = c

def neighbors(node):
    """
    Find neighboring nodes in A star search
    :param node: Parent
    :return:
    """
    global D,im,Elevation
    im = im.convert('RGB')
    pix = im.load()
    nbrs = list()
    # All 8 surrounding nodes
    nbrs.append(Node(node.x - 1, node.y-1,node))
    nbrs.append(Node(node.x - 1, node.y,node))
    nbrs.append(Node(node.x - 1, node.y + 1,node))
    nbrs.append(Node(node.x, node.y - 1,node))
    nbrs.append(Node(node.x, node.y + 1,node))
    nbrs.append(Node(node.x + 1, node.y + 1,node))
    nbrs.append(Node(node.x + 1, node.y,node))
    nbrs.append(Node(node.x + 1, node.y - 1,node))
    for i in nbrs:
        # Calculate linear distance
        d = math.sqrt(math.pow(((i.x - node.x) * 10.29), 2) + math.pow(((i.y - node.y) * 7.55), 2))
        # Calculate hypotenuse distance
        distance = math.sqrt(math.pow(Elevation[node.x][node.y]-Elevation[i.x][i.y],2) + math.pow(d,2))
        if pix[i.x, i.y] == (0,0,255):
            slope = 0
        else:
            slope = math.degrees(math.atan((Elevation[node.x][node.y] - Elevation[i.x][i.y])/d))
        if slope > 15:
            # Time taken to cross current pixel
            i.tt = distance / (D.get(pix[i.x, i.y],1) - (slope * 0.5))
        else:
            i.tt = distance / (D.get(pix[i.x, i.y], 1))
        i.parent = node
    return nbrs

def heuristic(st,gl):
    """
    Heuristic function which takes the perpendicular distance between 2 points at max speed
    :param st: Current pixel
    :param gl: Goal
    :return: Time taken at max speed
    """
    d = math.sqrt(math.pow(((st.x - gl.x) * 10.29), 2) + math.pow(((st.y - gl.y) * 7.55), 2))
    t = d/12
    return t

def pixneighbor(x,y,t,s):
    """
    Check neighbor pixels to find water pixel at edge
    :param x: x of pixel
    :param y: y of pixel
    :param t: RGB tuple to look for
    :param s: Season
    :return: True if pixel is at edge
    """
    global im,Elevation
    if s == 'Winter':
        if x > 0 and y > 0 and im.getpixel((x-1,y-1)) != t:
            return True
        elif x > 0 and im.getpixel((x-1,y)) != t:
            return True
        elif x > 0 and y < 499 and im.getpixel((x-1,y+1)) != t:
            return True
        elif y > 0 and im.getpixel((x,y-1)) != t:
            return True
        elif y < 499 and im.getpixel((x,y+1)) != t:
            return True
        elif x < 394 and y < 499 and im.getpixel((x+1,y+1)) != t:
            return True
        elif x < 394 and im.getpixel((x+1,y)) != t:
            return True
        elif x < 394 and y > 0 and im.getpixel((x+1,y-1)) != t:
            return True
        else:
            return False
    if s == 'Spring':
        if x > 0 and y > 0 and im.getpixel((x-1,y-1)) != t and Elevation[x-1][y-1] - Elevation[x][y] <= 1 :
            return True
        elif x > 0 and im.getpixel((x-1,y)) != t and Elevation[x-1][y] - Elevation[x][y] <= 1:
            return True
        elif x > 0 and y < 499 and im.getpixel((x-1,y+1)) != t and Elevation[x-1][y+1] - Elevation[x][y] <= 1:
            return True
        elif y > 0 and im.getpixel((x,y-1)) != t and Elevation[x][y-1] - Elevation[x][y] <= 1:
            return True
        elif y < 499 and im.getpixel((x,y+1)) != t and Elevation[x][y+1] - Elevation[x][y] <= 1:
            return True
        elif x < 394 and y < 499 and im.getpixel((x+1,y+1)) != t and Elevation[x+1][y+1] - Elevation[x][y] <= 1:
            return True
        elif x < 394 and im.getpixel((x+1,y)) != t and Elevation[x+1][y] - Elevation[x][y] <= 1:
            return True
        elif x < 394 and y > 0 and im.getpixel((x+1,y-1)) != t and Elevation[x+1][y-1] - Elevation[x][y] <= 1:
            return True
        else:
            return False

def constructpath(node):
   """
   Iterate back from goal node using parent variable
   :param node: Goal node
   :return: Path
   """
   path = list()
   path.append(node)
   while node.parent:
       node = node.parent
       path.append(node)
   return path

if __name__ == '__main__':
    main()